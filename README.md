TensorFlow 2.0 + Keras Overview for Deep Learning Researchers

*** forked from [A notebook giving you an overview of TensorFlow 2.0, by François Chollet, the creator of Keras](https://colab.research.google.com/drive/1UCJt8EYjlzCs1H1d1X0iDGYJsHKwu-NO#scrollTo=zoDjozMFREDU) ***

---

@fchollet, October 2019

This document serves as an introduction, crash course, and quick API reference for TensorFlow 2.0.

TensorFlow 2.0 is an extensive redesign of TensorFlow and Keras that takes into account over four years of user feedback and technical progress. It fixes the issues above in a big way.
It's a machine learning platform from the future.

TensorFlow 2.0 is built on the following key ideas:

* Let users run their computation eagerly, like they would in Numpy. This makes TensorFlow 22.0 programming intuitive and Pythonic.
* Preserve the considerable advantages of compiled graphs (for performance, distribution, an2d deployment). This makes TensorFlow fast, scalable, and production-ready.
* Leverage Keras as its high-level deep learning API, making TensorFlow approachable and highly productive.
* Extend Keras into a spectrum of workflows ranging from the very high-level (easier to use, less flexible) to the very low-level (requires more expertise, but provides great flexibility).
